; Downloads a fits file from VSO in the rice compression format.


PRO DownloadVSO, outDir

  ss = vso_search('2014-09-10T00:00:00','2014-09-10T01:00:00', instr='hmi', phys_obs='intensity',sample=3600)
  PRINT, 'Found ' +  string(ss.length, FORMAT='(I3)') + ' files. Downloading first file...'

  g = vso_get(ss[0], out_dir=outDir, /rice)

  PRINT, 'Done!'

END

