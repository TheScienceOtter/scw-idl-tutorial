import sunpy.map
import matplotlib.pyplot as plt
import configparser
from pathlib import Path


# Specify path to downloaded image
config_parser = configparser.ConfigParser()
config_parser.read('download_settings.ini')
path_to_fits = Path(config_parser.get('Settings', 'download_path'))
fits_files = [x for x in path_to_fits.glob('*.fits') if x.is_file()]

# Open the fits file and plot it, automatically constructing axes and titles
map = sunpy.map.Map(str(fits_files[0]))
map.plot()

# Use matplotlib to add colourbar and save
plt.colorbar()
plt.savefig('example_fits.png')