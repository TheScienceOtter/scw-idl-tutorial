**IDL users**


A simple set of example code to confirm that IDL and Solarsoft are functioning
as expected. The full procedure for getting this code up and running on the 
Supercomputing Wales systems is as follows:

  1. Download this project into your home directory: 
  
        `> git clone https://gitlab.com/TheScienceOtter/scw-idl-tutorial.git`
  2. Change into the directory:
  
        `> cd scw-idl-tutorial`
  3. Edit the set_sunbird.pro directories to point at our work directories. For 
     this tutorial we will just point it at the scw-idl-tutorial folder.

        `> nano set_sunbird.pro`
        
Change the home and idlhome lines to the following and then use CTRL+X to exit, 
pressing `y` when asked to save and then hitting enter to confirm the file name.
    
        home='/home/'+username+'/scw-idl-tutorial

        idlhome=home
    
  
  4. Load the IDL module:
  
        `> module load IDL`
  5. Run the idl command line program: 
  
        `> idl`
  6. Run the solarsoft configuration script:
  
        `> set_sunbird, ['sdo']`
  7. SSWIDL should now be configured. We can test this by using the example 
     procedure:  
        `> tutorialshowvsoimage, '/home/<USERNAME>/'`

     
More information about how to set up IDL can be found on the Supercomputing Wales
website: https://portal.supercomputing.wales/index.php/running-idl-jobs/


**Python and Sunpy**


Sunpy provides an alternative to SSWIDL for those who prefer the Python 
language. The python code provided allows you to download SDO data and display
it much like the IDL code. The benefit of using python over IDL in this instance
is that the python version is not limited to the number of available IDL
licenses. A few extra steps need to be taken to set up SunPy initially, however
these steps only need to be done once. 

  1. Load up the latest python3 module:
    
    > module load python/3.7.0

  2. Install the SunPy and Matplotlib libraries:
  
    > pip3 install --user sunpy
    
    > pip3 install --user matplotlib

These setup steps install the requirements for your user account and will be
loaded whenever you load the python modue in the future. The following steps 
explain how to run the scripts provided to download fits data and turn it into
a png image.

  1. If you haven't already, change into the project directory:
  
    > cd scw-idl-tutorial

  2. Open up the `download_settings.ini` file and change the output directory
     to your preferred location, then use CTRL+X to exit, pressing `y` when 
     asked to save and then hitting enter to confirm the file name:

    > nano download_settings.ini
    
  3. Run the downloader script first to obtain the fits file:
  
    > python3 VSODownloader.py

    This should show a readout of the parameters searched for and show a 
    progress bar while the file is downloading.
    
  4. After the file has been downloaded run the viewer script:
  
    > python3 sunpy_view_fits.py

The output image should be visible in the directory you specified in the
`download_settings.ini` file. This can be viewed either by viewing it through
VNC or by transferring the file to your desktop.


