import time

class PrintProgress(object):
    '''Manages the printing of progress of a loop to the standard output stream. Simply initialise the object outside of the loop
        by defining the start and end of the loop, then call the update function every loop. e.g.:

        prog = PrintProgress(0,100,interval=5,label="Process {0} at ".format(name))
        for i in range(0,100):
            DoSomething()
            prog.update()

        Version: 1.0.3
        Author: Richard Grimes
        E-Mail: rig12@aber.ac.uk
        Date: 2019-06-24
        '''

    def __init__(self, start, end, interval=10, label="Progress: "):
        '''Start an instance of the progress tracker.'''
        self.start = start
        self.end = end
        self.interval = interval
        self.label = label

        self.currentIteration = 0
        self.startTime = time.time()
        self.lastPrintPercent = 0
        self.update(0)

    def getETA(self):
        '''Get the ETA of finishing the process assuming a linear progression'''
        self.currentTime = time.time()
        self.deltaTime = (self.currentTime - self.startTime)
        self.deltaProgress = self.currentIteration  # (self.currentIteration - self.start)
        if self.deltaProgress == 0: self.deltaProgress = 1
        averageTime = self.deltaTime / self.deltaProgress
        return (self.end - self.currentIteration - self.start) * averageTime

    def formatETA(self, eta):
        '''Return a string of an ETA given in seconds into a HH:MM:SS format.'''
        m, s = divmod(eta, 60)
        h, m = divmod(m, 60)
        return "%d:%02d:%02d" % (h, m, s)

    def printProgress(self, percentProgress, eta):
        print("{0} {1}% | ETA: {2}".format(self.label, percentProgress, eta))

    def update(self, iterationsCompleted=1):
        '''Call this at the end of every iteration of the loop to determine whether or not a print statement should be issued'''
        self.currentIteration += iterationsCompleted
        try:
            percentProgress = round((self.currentIteration / (self.end - self.start)) * 100)
        except ZeroDivisionError:
            percentProgress = 100
        if percentProgress >= self.lastPrintPercent + self.interval or self.interval == -1 or iterationsCompleted == 0:
            self.lastPrintPercent = percentProgress
            eta = self.getETA()
            self.printProgress(percentProgress, self.formatETA(eta))

