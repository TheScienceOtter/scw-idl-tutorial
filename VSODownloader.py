from pathlib import Path
import astropy.units as u
from sunpy.net import Fido, attrs as a
from PrintProgress import PrintProgress
from datetime import datetime
import configparser
import sys

class Downloader:
    def __init__(self, path_to_config):
        self.config_parser = configparser.ConfigParser()
        self.config_parser.read(path_to_config)
        self.fits_dir = Path(self.config_parser.get('Settings','download_path'))

    def getFiles(self):
        """
        Searches VSO for files matching the parameters in the config file and then downloads them.
        :return:
        """
        print("[DownloadVSO] Searching for fits files...")
        time_start = datetime.strptime(self.config_parser.get('Settings', 'start_date'), '%Y-%m-%d_%H-%M-%S')
        time_end = datetime.strptime(self.config_parser.get('Settings', 'end_date'), '%Y-%m-%d_%H-%M-%S')
        instr = a.vso.Instrument(self.config_parser.get('Settings','instrument'))
        physobs = a.vso.Physobs(self.config_parser.get('Settings','observable'))
        cadence = a.vso.Sample(self.config_parser.getint('Settings','cadence') * u.second)
        time_step = (time_end - time_start)/100

        print("Searching for data matching the following parameters: "
                   "\nTime: {0} to {1}\nInstrument: {2}\nPhysObs: {3}\nCadence: {4}s".format(
                    time_start, time_end, instr, physobs, cadence))

        search_result = Fido.search(a.vso.Time(time_start, time_end), instr, physobs, cadence)
        print("[DownloadVSO] {0} files found. Beginning download...".format(search_result.file_num))

        # Use own progress tracker for large amounts of files because built-in tracker doesn't work
        # for non-interactive windows (like when running from a job file)
        if search_result.file_num >= 10:
            prog = PrintProgress(0,10,label="[DownloadVSO] Download progress...")
            for i in range(0,10):
                time = a.vso.Time(time_start + i * time_step,
                                  time_start + (i +1) * time_step)
                result = Fido.search(time, instr, physobs, cadence)
                downloaded_files = Fido.fetch(result, path=str(self.fits_dir), progress=False)
                if len(downloaded_files) == 0:
                    print("[DownloadVSO] Could not download file at time {0}. Searching for alternative...".format(time))
                prog.update()
        else:
            downloaded_files = Fido.fetch(search_result, path=str(self.fits_dir))

        files = [x for x in self.fits_dir.glob('*.fits') if x.is_file()]
        print("[DownloadVSO] {0} total files obtained.".format(len(files)))


if __name__ == "__main__":
    if len(sys.argv) <= 1:
        path_to_config = 'download_settings.ini'
    else:
        path_to_config = sys.argv[1]

    dl = Downloader(path_to_config)
    dl.getFiles()


