;+
; Sets up IDL and SSWIDL environments to run on Supercomputing Wales. 
; Edit your preferrred directories on the first 2 lines below the directories comment block, then run for required instruments.
; 
; example for IDL 8.7/8.4 SDO data:
;      set_sunbird, ['sdo']
;
;
; Author: Dr. Huw Morgan
; Version: 2020-02-21a
;-
pro set_sunbird,instusr,stereospice=stereospice


if n_params() gt 0 then inst=strlowcase(instusr) else inst=' '

spawn,'whoami',username

;===============================================================
; Directories - Change the paths below to suit your environment
;===============================================================
; your preferred working directory
home='/home/'+username+'/work'
; where you keep your own IDL programs
idlhome=home+'/progs2009'
;===============================================================

finfo=file_info(home)
fidlinfo=file_info(home)
if ~finfo.exists or ~fidlinfo.exists then begin
  print,'Please open program set_sunbird.pro and edit the two lines defining directories HOME and IDLHOME'
  return
endif

; Data directories
homessw='/apps/local/libraries/solarsoft';Solarsoft location
homedata='/home/SharedData/Aberystwyth';Solar data archive location
soho_data=homedata+'/soho'
sdo_data=homedata+'/sdo'
secchi_data=homedata+'/stereo/secchi'
processed_data=homedata+'/processed_data'
sswdb=homedata+'/solarsoft/data/sdb'

cd,home;change directory to working directory
setenv,'myidl='+idlhome

path_cache,/clear
pref_set,'IDL_PATH','<IDL_DEFAULT>',/commit;reset IDL !Path system variable
case strmid(!version.release,0,3) of 
  '8.7':PREF_SET, 'IDL_DLM_PATH', '/apps/local/languages/IDL/8.7/idl87/bin/bin.linux.x86_64:<IDL_DEFAULT>',/COMMIT
  '8.4':PREF_SET, 'IDL_DLM_PATH', '/apps/local/languages/IDL/8.4/idl84/bin/bin.linux.x86_64:<IDL_DEFAULT>',/COMMIT
  else:
endcase
!path=!path+';'+expand_path('+'+getenv('myidl'))

;solarsoft
ssw=homessw

setenv,'SOLARSOFT='+homessw
setenv,'SSW='+ssw
setenv,'HOME_DATA='+homedata
setenv,'PROCESSED_DATA='+processed_data
setenv,'SSWDB='+sswdb

dirs=['gen','vobs']

setenv,'SSW_ONTOLOGY='+ssw+'/vobs/ontology'
setenv,'SSW_JSOC='+getenv('SSW_ONTOLOGY')
setenv,'SSW_ONTOLOGY_DATA='+getenv('SSW_ONTOLOGY')+'/data'

for iinst=0,n_elements(inst)-1 do begin
  instnow=inst[iinst]
  if instnow eq ' ' then continue
  
  case instnow of
    'soho':begin
      dirs=[dirs,'soho','packages/sunspice','packages/nrl']
      setenv,'SOHO_DATA='+soho_data
      ;LASCO
      ssw_lasco=ssw+'/soho/lasco'
      setenv,'SSW_LASCO='+ssw_lasco
      !path=!path+';'+expand_path('+'+ssw_lasco)
      setenv,'NRL_LIB='+ssw_lasco
      setenv,'MONTHLY_IMAGES='+sswdb+'/soho/lasco/monthly'
      setenv,'LASCO_DATA='+ssw_lasco+'/idl/data'
      setenv,'ANCIL_DATA='+sswdb+'/soho/ancillary'
      defsysv,'!delimiter','/'
      ;EIT
      ssw_eit=ssw+'/soho/eit'
      setenv,'SSW_EIT='+ssw_eit
      !path=!path+';'+expand_path('+'+ssw_eit)
      setenv,'SSW_EIT_RESPONSE='+sswdb+'/soho/eit/response'
      
      setenv, 'SPICE_ICY_DLM='+homedata+'/solarsoft/icy/lib/icy.dlm'
      spice_soho=getenv('SSWDB')+'/soho/gen/spice'
      setenv,'SOHO_SPICE='+spice_soho
      setenv,'SOHO_SPICE_GEN='+ssw+'/soho/gen/data/spice'
      
     end
     
     'stereo':begin
      
        dirs=[dirs,'stereo','packages/sunspice']
      
        ;SECCHI
       ssw_secchi=ssw+'/stereo/secchi'
       setenv,'SECCHI_DATA='+secchi_data
       setenv,'SSW_SECCHI='+ssw_secchi
       !path=!path+';'+expand_path('+'+ssw_secchi)
       setenv,'SECCHI_BKG='+sswdb+'/stereo/secchi/backgrounds'
       setenv,'SECCHI_CAL='+ssw_secchi+'/calibration'
       ;setenv,'STEREO_SPICE='+ssw+'/stereo/gen/data/spice'
       setenv, 'SCC_DATA='+SSW_SECCHI+'/data'
       setenv, 'PT='+SSW_SECCHI+'/data/PT'
       setenv, 'SECCHI_LIB='+SSW_SECCHI+'/lib'
       setenv, 'EUVI_RESPONSE='+ssw_secchi+'/calibration/euvi_response'
       setenv,'SECCHI_DATA_AHEAD='+sswdb+'/stereo/secchi/L0/a'
       setenv,'SECCHI_DATA_BEHIND='+sswdb+'/stereo/secchi/L0/b'

       spice_stereo=ssw+'/stereo/gen/data/spice'
       setenv, 'STEREO_SPICE='+spice_stereo
       setenv, 'STEREO_SPICE_GEN='+spice_stereo+'/gen'
       setenv, 'STEREO_SPICE_SCLK='+spice_stereo+'/sclk'
       setenv, 'STEREO_SPICE_EPHEM='+spice_stereo+'/epm'
       setenv, 'STEREO_SPICE_DEF_EPHEM='+spice_stereo+'/depm'
       setenv, 'STEREO_SPICE_ATTIT='+spice_stereo+'/ah'
       setenv, 'STEREO_SPICE_ATTIT_SM='+spice_stereo+'/ah'
      
      end
     
    'sdo':begin
      dirs=[dirs,'sdo']
      ;SDO
      ssw_aia=ssw+'/sdo/aia'
      setenv,'SDO_DATA='+sdo_data
      setenv,'SSW_AIA='+ssw_aia
      setenv,'AIA_SYNOPTIC='+sdo_data+'/aia/synoptic'
      setenv,'AIA_CALIBRATION='+ssw_aia+'/calibration'
      setenv,'AIA_CALIBRATION_DATA='+getenv('AIA_CALIBRATION')+'/data'
      setenv,'DIR_SDO_PNT='+getenv('AIA_CALIBRATION_DATA')
      setenv,'AIA_RESPONSE_DATA='+ssw_aia+'/response'
     end
     
    'iris':begin
      dirs=[dirs,'iris']
      setenv,'IRIS_DATA='+homedata+'/iris'
    end
    
    'chianti':begin
      dirs=[dirs,'packages/chianti']
      ;CHIANTI
      chianti_dbase=ssw+'/packages/chianti.20170124/dbase'
      defsysv,'!xuvtop',chianti_dbase
      defsysv,'!ioneq_file',chianti_dbase+'/ioneq/mazzotta_etal.ioneq'
      defsysv,'!abundance_file',chianti_dbase+'/abundance/sun_coronal.abund';allen
      defsysv,'!abund_file',chianti_dbase+'/abundance/sun_coronal.abund'
      defsysv,'!dem_file',chianti_dbase+'/dem/coronal_home.dem'
      defsysv,'!masterlist_file',chianti_dbase+'/masterlist/masterlist.ions.v3'
     end
     
    'hinode':begin
      dirs=[dirs,'hinode']
      ;HINODE
      setenv,'HINODE_DATA='+homedata+'/hinode'
      ;EIS
      ssw_eis=ssw+'/hinode/eis'
      setenv, 'SSW_EIS='+ssw_eis
      ;setenv EIS_INST "ITA-UiO"
      eis_data=ssw_eis+'/data'
      setenv, 'EIS_DATA='+eis_data

      setenv, 'EIS_CCSDS_DATA='+eis_data+'/science/ccsds'
      ;setenv EIS_FITS_DATA $EIS_DATA/science/fits
      setenv, 'EIS_HK_DATA='+eis_data+'/status'
      setenv, 'EIS_CAL_DATA='+eis_data+'/cal'
      setenv, 'EIS_RESPONSE='+ssw_eis+'/response'
      setenv, 'EIS_BIN='+ssw_eis+'/bin'
      setenv, 'EIS_STATUS_FITS_KEYWORDS='+ SSW_EIS+'/idl/spacecraft/housekeeping/fits/keywords'
    end
    
    'hessi':begin
      dirs=[dirs,'hessi']
      setenv, 'HESSI_PATH='+ SSW+'/hessi'
      setenv, 'HSI_FLARE_LIST_ARCHIVE='+  SSW+'/hessi/dbase
     end
    
    else:begin
      print,'Instrument/package ',instnow,' not recognised (set_enviroments)'
    end
  endcase
endfor

for idir=0,n_elements(dirs)-1 do begin
  dnow=getenv('SSW')+'/'+dirs[idir]
  dirsnow=file_search(dnow,'idl',count=nidl,/test_directory)
  for i=0,nidl-1 do !path=!path+':'+expand_path('+'+dirsnow[i])
  dirsnow=file_search(dnow,'idl_libs',count=nidl,/test_directory)
  for i=0,nidl-1 do !path=!path+':'+expand_path('+'+dirsnow[i])
endfor

devicelib
imagelib

indstereo=where(strmatch(inst,'stereo'),cntstereo)
if cntstereo gt 0 then begin
  register_stereo_spice_dlm
  if keyword_set(stereospice) then begin
    load_stereo_spice
    load_stereo_spice_gen
    print,'Loaded stereo spice sorry about the wait'
  endif
endif

indsoho=where(strmatch(inst,'soho'),cntsoho)
if cntsoho gt 0 then begin
  load_sunspice_soho
  ;this is probably a bad workaround, but soho spice won't load unless I call stereo first
  ;if cntstereo eq 0 then begin
   ; set_sunbird,['stereo','soho']
  ;endif
  ;load_sunspice_soho
endif

copy_files=[getenv("SSW")+'/gen/idl/util/uniq.pro', $
            getenv("SSW")+'/gen/idl/util/minmax.pro', $
            getenv("SSW")+'/gen/idl/util/datatype.pro', $
            getenv("SSW")+'/gen/idl/help/dprint.pro']
for i=0,n_elements(copy_files)-1 do $
    file_copy,copy_files[i],'./'+file_basename(copy_files[i]),/over,/force


end
