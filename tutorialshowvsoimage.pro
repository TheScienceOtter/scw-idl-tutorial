; An example procedure that downloads an image using DownloadVSO.pro and then 
; saves the image data to a .png file inthe specified output directory (outDir)
;
; These procedures use VSO solarsoft commands to read and display fits data. If
; these procedures work then it means your solarsoft library is installed and
; configured correctly.
;
; !!!! Don't forget to run the set_sunbird.pro provided by Huw first !!!!

PRO TutorialShowVSOImage, outDir

  ; Download an image from VSO
  DownloadVSO, outDir
  
  ; Read the first fits image in the directory.
  if not outDir.endswith('/') THEN BEGIN 
    outDir = outDir + '/'
  ENDIF  
  ff = file_search(outDir + '*.fits')
  read_sdo, ff[0], header, data
  
  ; Save image without displaying it
  img = IMAGE(data, /BUFFER)
  img.save, outDir + 'output.png'
  img.close
  
  PRINT, 'Saved image to ' + outDir

END
